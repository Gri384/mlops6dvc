FROM mambaorg/micromamba

WORKDIR /app 
COPY . /app

# Создание и активация окружения для разработки машинного обучения
RUN micromamba create -f env.yml
