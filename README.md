## Домашнее задание 6 блока MLOps-3 - DVC

доступ к данным в пайплайне предоставлен через сервисный аккаунт Google.

В репозитории 2 ветки: [master (ссылка)](https://gitlab.com/Gri384/mlops6dvc) и [random_forest (ссылка)](https://gitlab.com/Gri384/mlops6dvc/-/tree/random_forest?ref_type=heads)
В ветке *master* для исследования использовалась модель логистической регрессии, которая показала лучший результат, по сравнению  с random_forest.
    [ссылка на pipeline](https://gitlab.com/Gri384/mlops6dvc/-/jobs/6819240128)
В ветке *random_forest* для исследования использовалась модель random_forest.
    [ссылка на pipeline](https://gitlab.com/Gri384/mlops6dvc/-/jobs/6829619996)
