import re
from pathlib import Path

import click
import nltk
import polars as pl
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

from cli import cli

nltk.download("wordnet")
nltk.download("stopwords") 

def text_preprocessing(input_text: str) -> str:
    stop_words = stopwords.words("english")
    url_pattern = re.compile(r"https?://\S+|www\.\S+|\[.*?\]|[^a-zA-Z\s]+|\w*\d\w*")
    spec_chars_pattern = re.compile("[0-9 \-_]+")
    non_alpha_pattern = re.compile("[^a-z A-Z]+")
    text = input_text.lower()
    text = url_pattern.sub("", text)
    text = spec_chars_pattern.sub(" ", text)    # убираем спец символы
    text = non_alpha_pattern.sub(" ", text)     # оставляем только буквы
    text = " ".join(word for word in text.split() if word not in stop_words)
    return text.strip()


def lemmatize(input_frame: pl.DataFrame) -> pl.DataFrame:
    lemmatizer = WordNetLemmatizer()

    return input_frame.with_columns(
        pl.col("corpus").map_elements(
            lambda input_list: [lemmatizer.lemmatize(token) for token in input_list]
        )
    )


def dataframe_preprocessing(data: pl.DataFrame, col_name: str) -> pl.DataFrame:
    return lemmatize(
        data.with_columns(
            pl.col(col_name)
            .map_elements(text_preprocessing)
            .str.split(" ")
            .alias("corpus")
        )
    )


@cli.command()
@click.argument("input_frame_path", type=Path)
@click.argument("output_frame_path", type=Path)
@click.argument("column", type=str, default="Review")
def cli_preprocessing(input_frame_path: Path, output_frame_path: Path, column: str):
    data = pl.read_csv(
        input_frame_path,
        has_header=False,
        new_columns=["Polarity", "Title", "Review"],
        n_rows=50000,
    )
    processed_data = dataframe_preprocessing(data, column)
    processed_data.write_parquet(output_frame_path)
